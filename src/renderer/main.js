import Vue from 'vue'
import axios from 'axios'

import App from './App'
import router from './router'
import store from './store'

//import jQuery from 'jquery'
import BootstrapVue from 'bootstrap-vue'

import VueFlashMessage from 'vue-flash-message'

window.jQuery = window.$ = require('jquery')
//import 'bootstrap/dist/js/bootstrap.min.js'
import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import fontawesome from '@fortawesome/fontawesome'
import regular from '@fortawesome/fontawesome-free-regular'
import brands from '@fortawesome/fontawesome-free-brands'
import solid from '@fortawesome/fontawesome-free-solid'

import VeeValidate from 'vee-validate'
import VueQuillEditor from 'vue-quill-editor'
import VModal from 'vue-js-modal'
import ToggleButton from 'vue-js-toggle-button'

import 'quill/dist/quill.core.css'
import 'quill/dist/quill.snow.css'
import 'quill/dist/quill.bubble.css'

// Require Froala Editor js file.
require('froala-editor/js/froala_editor.pkgd.min')

// Require Froala Editor css files.
require('froala-editor/css/froala_editor.pkgd.min.css')
require('font-awesome/css/font-awesome.css')
require('froala-editor/css/froala_style.min.css')

// Import and use Vue Froala lib.
import VueFroala from 'vue-froala-wysiwyg'

fontawesome.library.add(regular, brands, solid)

if (!process.env.IS_WEB) Vue.use(require('vue-electron'))
Vue.http = Vue.prototype.$http = axios
Vue.config.productionTip = false
Vue.use(VeeValidate)
Vue.use(VueFlashMessage, {
	messageOptions: {
      createShortcuts: false,
    	timeout: 3000,
    	pauseOnInteract: true,
    	onStartInteract: doNothing(),
    	onCompleteInteract: doNothing()
  }
})
Vue.use(VueQuillEditor)
Vue.use(VModal, { componentName: "vu-modal" })
Vue.use(BootstrapVue)
Vue.use(require('vue-moment'))
Vue.use(ToggleButton)
Vue.use(VueFroala)

function doNothing() {
	return
}

/* eslint-disable no-new */
new Vue({
  components: { App },
  router,
  store,
  template: '<App/>',
  filters: {
    unescape: v => unescape(v)
  }
}).$mount('#app')

router.beforeEach((to, from, next) => {
  window.scrollTo(0,0)
  next()
})