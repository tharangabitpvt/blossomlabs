import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/splash_page',
      name: 'splash-page',
      component: require('@/components/SplashPage').default
    },
    {
      path: '/first_app_setup',
      name: 'one-time-setup',
      component: require('@/components/SystemSetup').default
    },
    {
      path: '/',
      name: 'login-page',
      component: require('@/components/LoginPage').default
    },
    {
      path: '/landing_page',
      name: 'landing-page',
      component: require('@/components/LandingPage').default
    },
    {
      path: '/edit_invoice/:invoice_no',
      name: 'edit-invoice',
      component: require('@/components/LandingPage').default
    },
    {
      path: '/dashboard',
      name: 'admin-dashboard',
      component: require('@/components/Admin/AdminDashboard').default
    },
    {
      path: '/user',
      name: 'manage-users',
      component: require('@/components/User').default
    },
    {
      path: '/edit_user/:id',
      name: 'edit-user',
      component: require('@/components/UserEdit').default
    },
    {
      path: '/edit_user',
      name: 'add-user',
      component: require('@/components/UserEdit').default
    },
    {
      path: '/tests',
      name: 'manage-tests',
      component: require('@/components/MediTests').default
    },
    {
      path: '/edit_test/:id',
      name: 'edit-test',
      component: require('@/components/MediTestEdit').default
    },
    {
      path: '/edit_test',
      name: 'add-test',
      component: require('@/components/MediTestEdit').default
    },
    {
      path: '/test_profiles',
      name: 'manage-test-profiles',
      component: require('@/components/TestProfile').default
    },
    {
      path: '/add_test_profile',
      name: 'add-test-profile',
      component: require('@/components/TestProfileEdit').default
    },
    {
      path: '/edit_test_profile/:id',
      name: 'edit-test-profile',
      component: require('@/components/TestProfileEdit').default
    },
    {
      path: '/profile_group',
      name: 'manage-profile-groups',
      component: require('@/components/ProfileGroup').default
    },
    {
      path: '/edit_profile_group/:id',
      name: 'edit-profile-group',
      component: require('@/components/ProfileGroupEdit').default
    },
    {
      path: '/add_profile_group/',
      name: 'add-profile-group',
      component: require('@/components/ProfileGroupEdit').default
    },
    {
      path: '/manage_collecting_centers',
      name: 'manage-collecting-centers',
      component: require('@/components/CollectingCenter').default
    },
    {
      path: '/add_collecting_center',
      name: 'add-collecting-center',
      component: require('@/components/CollectingCenterEdit').default
    },
    {
      path: '/edit_collecting_center/:id',
      name: 'edit-collecting-center',
      component: require('@/components/CollectingCenterEdit').default
    },
    {
      path: '/test_units',
      name: 'test-units',
      component: require('@/components/MediTestUnits').default
    },
    {
      path: 'fluid_types',
      name: 'fluid-types',
      component: require('@/components/FluidTypes').default
    },
    {
      path: 'ext_labs',
      name: 'external-labs',
      component: require('@/components/ExternalLabs').default
    },
    {
      path: '/manage_doctors',
      name: 'manage-doctors',
      component: require('@/components/Doctor').default
    },
    {
      path: '/add_doctors',
      name: 'add-doctor',
      component: require('@/components/DoctorEdit').default
    },
    {
      path: '/edit_doctors/:id',
      name: 'edit-doctor',
      component: require('@/components/DoctorEdit').default
    },
    {
      path: '/system_settings',
      name: 'system-settings',
      component: require('@/components/SystemSettings').default
    },
    {
      path: '/add_test_results',
      name: 'add-test-results',
      component: require('@/components/AddTestResults').default
    },
    {
      path: '/customers',
      name: 'manage-customers',
      component: require('@/components/Customer').default
    }, 
    {
      path: '/edit_customer/:id',
      name: 'edit-customer',
      component: require('@/components/CustomerEdit').default
    },
    {
      path: '/customer_details/:id',
      name: 'customer-detail',
      component: require('@/components/CustomerDetail').default
    },
    {
      path: '/stock_items',
      name: 'stock-items',
      component: require('@/components/StockItems').default
    },
    {
      path: '/stock_items_edit/:id',
      name: 'stock-item-edit',
      component: require('@/components/StockItemEdit').default
    },
    {
      path: '/stock_items_edit',
      name: 'add-stock-item',
      component: require('@/components/StockItemEdit').default
    },
    {
      path: '/manage_packages',
      name: 'manage-packages',
      component: require('@/components/Packages').default
    },
    {
      path: '/edit_packages/:id',
      name: 'package-edit',
      component: require('@/components/PackageEdit').default
    },
    {
      path: '/add_packages',
      name: 'add-package',
      component: require('@/components/PackageEdit').default
    },
    {
      path: '/manage_antibiotics',
      name: 'manage-antibiotics',
      component: require('@/components/Antibiotics').default
    },
    {
      path: '/edit_antibiotic/:id',
      name: 'edit-antibiotic',
      component: require('@/components/AntibioticEdit').default
    },
    {
      path: '/add_antibiotic',
      name: 'add-antibiotic',
      component: require('@/components/AntibioticEdit').default
    },
    {
      path: '/print_invoice/:invoice_no/:print_mode',
      name: 'print-invoice',
      component: require('@/components/PrintInvoice').default
    },
    {
      path: '/print_invoice_bl/:invoice_no/:print_mode',
      name: 'print-invoice-bl',
      component: require('@/components/Print/Invoice').default
    },
    {
      path: '/print_test_report/:invoice_test_id/:print_mode',
      name: 'print-test-report',
      component: require('@/components/Print/TestReport').default
    },
    {
      path: '/print_test_report_bl/:invoice_test_id/:print_mode',
      name: 'print-test-report-bl',
      component: require('@/components/Print/TestReportBL').default
    },
    {
      path: '/print_test_report_group/:invoice_id/:group_id/:print_mode',
      name: 'print-test-report-group',
      component: require('@/components/Print/TestReportGroup').default
    },
    {
      path: '/print_test_report_group_bl/:invoice_id/:group_id/:print_mode',
      name: 'print-test-report-group-bl',
      component: require('@/components/Print/TestReportGroupBL').default
    },
    {
      path: '/print_sys_report/:chart_data',
      name: 'print-sys-report',
      component: require('@/components/Print/PrintSysReport').default
    },    
    {
      path: '/print_worksheet/:invoice_id',
      name: 'print-worksheet',
      component: require('@/components/Print/LabWorksheet').default
    },
    {
      path: '/save_print_layout/:layout_type/:page_size',
      name: 'save-print-layout',
      component: require('@/components/Print/Layouts/Invoice').default
    },
    {
      path: '/save_print_layout_medi_report/:layout_type/:page_size',
      name: 'save-print-layout-medi-report',
      component: require('@/components/Print/Layouts/Report').default
    },
    {
      path: '/app_activation',
      name: 'bl-activation',
      component: require('@/components/BL').default
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
})
