const state = {
	isSettingChanged: false
}

const mutations = {
  settingChanged (state) {
    state.isSettingChanged = true
  },
  resetState (state) {
    state.isSettingChanged = false
  }
}

const actions = {
    settingChanged ({ commit }) {
    // do something async
    commit('settingChanged');
    setTimeout(function(){ commit('resetState'); }, 3000);
  }
}

export default {
    namespaced: true,
    state,
    mutations,
    actions
}