const state = {
	triggerWatch: false
}

const mutations = {
  SET_WATCH_TRIGGER (state) {
    state.triggerWatch = true
  },
  RESET_WATCH (state) {
    state.triggerWatch = false
  }
}

const actions = {
  setWatchTrigger ({ commit }) {
    // do something async
    commit('SET_WATCH_TRIGGER')
  },
  resetWatch({ commit }) {
    commit('RESET_WATCH')
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}