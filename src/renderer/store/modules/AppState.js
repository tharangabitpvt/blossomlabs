const state = {
	appState: 'ready',
  printWithMargin: true,
  bcsEnabled: false,
  mainStationConnected: false,
  trialDays: 0,
  activationState: false,
  backupDone: false
}

const mutations = {
  SET_APP_STATE (state, val) {
    state.appState = val
  },
  SET_PRINT_WITH_MARGIN (state, val) {
    state.printWithMargin = val
  },
  SET_BCS_STATE (state, val) {
    state.bcsEnabled = val
  },
  SET_MAIN_STATION_CON (state, val) {
    state.mainStationConnected = val
  },
  SET_TRIAL_DAYS (state, val) {
    state.trialDays = val
  },
  SET_ACTIVATION_STATE (state, val) {
    state.activationState = val
  },
  SET_BACKUP_DONE (state, val) {
    state.backupDone = val
  }
}

const actions = {
  setAppState ({ commit }, appstateVal) {
    // do something async
    commit('SET_APP_STATE', appstateVal)
  },
  setPrintWithMargin ({ commit }, val) {
    commit('SET_PRINT_WITH_MARGIN', val)
  },
  setBcsState ({ commit }, val) {
    commit('SET_BCS_STATE', val)
  },
  setMainStationCon ({commit}, val) {
    commit('SET_MAIN_STATION_CON', val)
  },
  setTrialDays ({ commit }, val) {
    commit('SET_TRIAL_DAYS', val)
  },
  setActivationState ({ commit }, val) {
    commit('SET_ACTIVATION_STATE', val)
  },
  setBackupDone ({commit}, val) {
    commit('SET_BACKUP_DONE', val)
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}