const state = {
	pendingTestCount: 0
}

const mutations = {
  SET_PENDING_TEST_COUNT (state, val) {
    state.pendingTestCount = val
  },
  DECREMENT_COUNT (state) {
  	if(state.pendingTestCount > 0) {
  		state.pendingTestCount--
  	}
  },
  INCRMENT_COUNT (state) {
  	state.pendingTestCount++
  }
}

const actions = {
  setPendingTestCount ({ commit }, val) {
    // do something async
    commit('SET_PENDING_TEST_COUNT', val)
  },
  decrementCount ({commit}) {
  	commit('DECREMENT_COUNT')
  },
  incrementCount({commit}) {
  	commit('INCRMENT_COUNT')
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}