const grayBackground = '#eff1f1'
const buttonBackgroundColor = '#53676c'
const inactiveTextColot = '#607D8B'
const textColor = '#374548'
const lightBgColor = '#e8eced'

import { app, BrowserWindow, webContents, session, ipcMain, dialog, Tray, shell, Menu } from 'electron'

//app.commandLine.appendSwitch('inspect', '5858')

const url = require('url')
const path = require('path')
const fs = require('fs')
const fse = require('fs-extra')
const crypto = require('crypto')
const fetch = require('electron-fetch')
const isOnline = require('is-online')
const mysql = require('mysql')
const Store = require('electron-store')
const store = new Store({encryptionKey: 'asdiuher%A*sda2@sdfj*(sd#asdlknvh'})
//const PDFWindow = require('electron-pdf-window')

//store.clear()
const ftAppDataDir = app.getPath('userData')
const ftErrorLogFile = path.join(ftAppDataDir, 'logs', 'error_log.txt') 
const appConfigJsonPath = process.env.NODE_ENV !== 'development' ? path.join(__dirname, 'static', 'app_config.json') : path.join(__static, 'app_config.json')
const mainConfigJsonPath = process.env.NODE_ENV !== 'development' ? path.join(__dirname, 'static', 'config.json') : path.join(__static, 'config.json')
//const hepPDFFile = process.env.NODE_ENV !== 'development' ? path.join(__dirname, 'static', 'BlossomLabsManual.pdf') : path.join(__static, 'BlossomLabsManual.pdf')

let appConfigJson = JSON.parse(fse.readFileSync(appConfigJsonPath))
let mainConfigJson = JSON.parse(fse.readFileSync(mainConfigJsonPath))

const trialPeriod = 86400000*30; // 30 days

//store.delete('main_config')
//store.delete('app-config')
//console.log('app config deleted')

var displayErrors = store.get('system_settings') && store.get('system_settings').bl_settings && store.get('system_settings').bl_settings.enableDisplayErrors

//// ROMOVE FOLLOWING COMMENTED 'IF' BLOCK ON PRODUTION //////
if(!store.has('app-config')) {
    store.set('app-config', appConfigJson)
}

if(!store.has('main_config')) {
    store.set('main_config', mainConfigJson)
}

var mainConfig = store.get('main_config')

/*process.on('uncaughtException', function (error) {
    console.log('Error: ' + error)
    logError('Uncought error:\n' + error)
    dialog.showMessageBox({
        buttons: ['Quit'],
        type: 'error',
        title: 'Unexpected error',
        message: 'Unexpected error occurred',
        detail: 'Please quit and relaunch the app. \nIf you see this message continuously, please contact BlossomLabs for assistant.'
    }, function(btnIndex){
        app.exit(0)        
    })

  // return
});*/

console.log('electron verion', process.versions.electron)
/**
 * Set `__static` path to static files in production
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-static-assets.html
 */
if (process.env.NODE_ENV !== 'development') {
  global.__static = require('path').join(__dirname, '/static').replace(/\\/g, '\\\\')
}

let mainWindow
let splashWindow
var winURL = process.env.NODE_ENV === 'development'
    ? `http://localhost:9080`
    : `file://${__dirname}/index.html`

if(!mainConfig || !mainConfig.setup) {
winURL = process.env.NODE_ENV === 'development'
    ? `http://localhost:9080/#/first_app_setup`
    : `file://${__dirname}/index.html#first_app_setup`
}

var splashUrl = process.env.NODE_ENV === 'development'
    ? `http://localhost:9080/#/splash_page`
    : `file://${__dirname}/index.html#splash_page`


function createWindow () {
    /**
    * Initial window options
    */
    let {width, height} = require('electron').screen.getPrimaryDisplay().size
    mainWindow = new BrowserWindow({
        title: "BlossomLabs", 
        fullscreen: false, 
        backgroundColor: grayBackground, 
        width: width, 
        height: height, 
        minWidth: (width-100), 
        minHeight: (height-100), 
        toolbar:true,
        show: false
    })

    if(process.env.NODE_ENV === 'production' && displayErrors) {
        mainWindow.webContents.openDevTools()
    }
    
    mainWindow.on('close', (event) => {
        const colseAppDialog = dialog.showMessageBox(
            mainWindow,
            {buttons:['Yes', 'No'], 
            title: 'Quit BlossomLabs', 
            type: 'question', 
            message: 'Exit from BlossomLabs?', 
            detail: 'Do you want to quit from BlossomLabs now?', defaultId: 0, cancelId: 1},
        )

        if(colseAppDialog == 1) {
          event.preventDefault()
        } else {
          //connection.destroy()
          store.set('isLogedIn', false)
          store.delete('user')
          app.exit(0)
        }
    })

    splashWindow = new BrowserWindow({
        title: 'BlossomLabs', 
        fullscreen: false, 
        backgroundColor: '#dee5e6', 
        width: 500, 
        height: 300,
        center: true,
        parent: mainWindow,
        modal: true,
        resizable: false,
        frame: false,
        toolbar:false,
        show: false
    })

    if(process.env.NODE_ENV === 'production' && displayErrors) {
        splashWindow.webContents.openDevTools()
    }

    // SocketIO
    if(mainConfig.system_type == "server") {
        // server app
        var server = require('http').createServer()
        var io = require('socket.io')(server)
        io.on('connection', function(sock){

            //wakeup emit. This message is sent out if there are sub systems waiting for Main station connection.
            // Eg: Main station restarts accidantally etc...
            sock.emit('wakeup', true)
            
            ipcMain.on('invoice-count', (event, args) => {
                //console.log('invoice count from renderer to send to sub', args)
                if(args) {
                    sock.emit('invoice-count', args)
                }
            })

            sock.on('invoice-count-client', function(inviceCount){
                //console.log('invoice count from sub system', inviceCount)
                mainWindow.webContents.send('new-invoice-count', inviceCount)
            })

            sock.on('fetch-details', function(fetchFor){
                var fetchReply = null
                if(fetchFor == 'lab-info') {
                    var labInfoObj = store.get('system_settings').lab_info
                    fetchReply = labInfoObj
                }

                if(fetchFor == 'con-test') {
                    fetchReply = JSON.stringify({'con_test': true})
                }

                sock.emit('fetch-reply', fetchReply)
            })

            sock.on('check-activation-client', function() {
                var activationStatus = checkForActivation()
                sock.emit('activation-resp', JSON.stringify(activationStatus))
            })

            sock.on('disconnect', function(){

            })
        })
        server.listen(3000)
    } else {
        // client app
        const sioc = require("socket.io-client")
        var ioClient = sioc.connect(`http://${mainConfig.server}:3000`)

        ipcMain.on('invoice-count-client', (event, newInvoiceCount) => {
            ioClient.emit('invoice-count-client', newInvoiceCount)
        })

        ioClient.on('invoice-count', (invoiceCount) => {
            // send ipc command to increment pending count
            if(invoiceCount !== null) {
                mainWindow.webContents.send('new-invoice-count', invoiceCount)
            }
        })

        ioClient.on('disconnect', (reason) => {
            if (reason === 'io server disconnect') {
                // the disconnection was initiated by the server, you need to reconnect manually
                ioClient.connect();
            }
          // else the socket will automatically try to reconnect
        })

        ioClient.on('activation-resp', status => {
            mainWindow.webContents.send('app-activation', status)
        })

        ipcMain.on('check-activation', (event, args) => {
            ioClient.emit('check-activation-client')
        })
    }

    if(mainConfig.setup) {
        splashWindow.loadURL(splashUrl)

        splashWindow.once('ready-to-show', () => {
            mainWindow.show()
            splashWindow.show()                
        })

        let {width, height} = require('electron').screen.getPrimaryDisplay().size

        ipcMain.on('app-init-status', (event, appInitStatus) => {
            if(appInitStatus) {
                
                // load Main window content if everything is OK, and hide Splah window
                mainWindow.loadURL(winURL)
                mainWindow.webContents.on('did-finish-load', () => {
                    splashWindow.hide()
                })

                if(mainConfig && mainConfig.setup) {

                    var pool = mysql.createPool({
                        host     : mainConfig.server,
                        user     : mainConfig.db_user,
                        password : mainConfig.db_pwd,
                        database : mainConfig.db_name
                    })

                    ipcMain.on('db-query', (event, args) => {
                        var queryObj = JSON.parse(`${args}`)

                        //connection.connect()
                        pool.getConnection(function(err, connection) {
                            if(err) {
                                logError('MySQL Error:\r\n' + err)
                                console.log(err)
                                throw err
                                return
                            }
                            // Use the connection
                            connection.query(queryObj.query, queryObj.params, function (error, results, fields) {
                                // And done with the connection.
                                connection.release();

                                // Handle error after the release.
                                if (error) {
                                    if(error.code === 'ER_DUP_ENTRY') {
                                        event.returnValue = {error: error.code}
                                    } else {
                                        logError('MySQL Error:\r\n' + error + '\r\n' + queryObj.query)
                                        console.log('Error code:', error.code)
                                        throw error                                        
                                    } 

                                    return                                  
                                }

                                // Don't use the connection here, it has been returned to the pool.
                                event.returnValue = results
                            })
                        })
                    })

                    ipcMain.on('print-job', (event, printParams) => {
                        var requestParams = JSON.parse(`${printParams}`)
                        
                        var printLayoutURL = ""
                        var PDFFileName = ""
                        var subDirectory = ""
                        var printBackground = false
                        if(requestParams.printDoc == 'invoice') {
                            if(store.get('system_settings') && store.get('system_settings').print_settings && store.get('system_settings').print_settings.useComputerForms) {
                                printLayoutURL = process.env.NODE_ENV === 'development'
                                ? `http://localhost:9080/#/print_invoice_bl/${requestParams.printData.invoice_no}/${requestParams.printMode}`
                                : `file://${__dirname}/index.html#print_invoice_bl/${requestParams.printData.invoice_no}/${requestParams.printMode}`
                            } else {
                                printLayoutURL = process.env.NODE_ENV === 'development'
                                ? `http://localhost:9080/#/print_invoice/${requestParams.printData.invoice_no}/${requestParams.printMode}`
                                : `file://${__dirname}/index.html#print_invoice/${requestParams.printData.invoice_no}/${requestParams.printMode}`
                            }

                            PDFFileName = `${requestParams.printData.invoice_no}`
                            subDirectory = 'Invoices'
                        } else if(requestParams.printDoc == 'test_report') {
                            if(requestParams.printData.group_id) {
                                if(requestParams.printMode != 'pdf' && store.get('system_settings') && store.get('system_settings').print_settings && store.get('system_settings').print_settings.useComputerFormsMediReports) {
                                    printLayoutURL = process.env.NODE_ENV === 'development'
                                    ? `http://localhost:9080/#/print_test_report_group_bl/${requestParams.printData.invoice_id}/${requestParams.printData.group_id}/${requestParams.printMode}`
                                    : `file://${__dirname}/index.html#print_test_report_group_bl/${requestParams.printData.invoice_id}/${requestParams.printData.group_id}/${requestParams.printMode}`
                                } else {
                                    printLayoutURL = process.env.NODE_ENV === 'development'
                                    ? `http://localhost:9080/#/print_test_report_group/${requestParams.printData.invoice_id}/${requestParams.printData.group_id}/${requestParams.printMode}`
                                    : `file://${__dirname}/index.html#print_test_report_group/${requestParams.printData.invoice_id}/${requestParams.printData.group_id}/${requestParams.printMode}`
                                }
                            } else {
                                if(requestParams.printMode != 'pdf' && store.get('system_settings') && store.get('system_settings').print_settings && store.get('system_settings').print_settings.useComputerFormsMediReports) {
                                    printLayoutURL = process.env.NODE_ENV === 'development'
                                    ? `http://localhost:9080/#/print_test_report_bl/${requestParams.printData.invoice_test_id}/${requestParams.printMode}`
                                    : `file://${__dirname}/index.html#print_test_report_bl/${requestParams.printData.invoice_test_id}/${requestParams.printMode}`
                                } else {
                                    printLayoutURL = process.env.NODE_ENV === 'development'
                                    ? `http://localhost:9080/#/print_test_report/${requestParams.printData.invoice_test_id}/${requestParams.printMode}`
                                    : `file://${__dirname}/index.html#print_test_report/${requestParams.printData.invoice_test_id}/${requestParams.printMode}`
                                }                                
                            }

                            subDirectory = 'Test reports'
                            
                            if(requestParams.printData.group_name) {
                                PDFFileName = `test_report_${requestParams.printData.invoice_no}_${requestParams.printData.group_name}`
                            } else {
                                PDFFileName = `test_report_${requestParams.printData.invoice_no}_${requestParams.printData.profile_name}`
                            } 
                        } else if(requestParams.printDoc == 'sys_report') {
                            printLayoutURL = process.env.NODE_ENV === 'development'
                            ? `http://localhost:9080/#/print_sys_report/${encodeURIComponent(requestParams.printData.chart_data)}`
                            : `file://${__dirname}/index.html#print_sys_report/${encodeURIComponent(requestParams.printData.chart_data)}`

                            subDirectory = 'System reports'
                            PDFFileName = requestParams.printData.pdf_file_name //'system_report.pdf'
                            printBackground = !requestParams.printerFrienldy

                        } else if(requestParams.printDoc == 'worksheet') {
                            printLayoutURL = process.env.NODE_ENV === 'development'
                            ? `http://localhost:9080/#/print_worksheet/${requestParams.printData.invoice_id}`
                            : `file://${__dirname}/index.html#print_worksheet/${requestParams.printData.invoice_id}`

                            subDirectory = "worksheets"
                            PDFFileName = `worksheet_${requestParams.printData.invoice_id}`
                        }
                        //console.log(PDFFileName)
                        
                        let prevWindow = new BrowserWindow({
                            title: "Print Preview",
                            fullscreen: false, 
                            backgroundColor: '#fff',
                            width: 700, 
                            height: height,
                            toolbar: false,
                            show: false
                        })

                        prevWindow.webContents.on('did-finish-load', () => {
                            //prevWindow.show()

                            if(requestParams.printMode == 'pdf') {
                                var pdfSavePath = app.getPath('desktop')
                                if(store.get('system_settings').report_settings.savePath) {
                                    pdfSavePath = store.get('system_settings').report_settings.savePath
                                }

                                // create directory if not exists
                                var fileSaveDirPath = path.join(pdfSavePath, subDirectory)
                                if(!fse.ensureDirSync(fileSaveDirPath)) {
                                    fse.mkdirsSync(fileSaveDirPath)
                                }

                                prevWindow.webContents.printToPDF({printBackground: printBackground}, (error, data) => {
                                    if(error) {
                                        logError('Print TO PDF Error:\r\n' + error)
                                        throw error
                                    }

                                    var cleanPDFFileName = PDFFileName.replace(/[^a-z0-9_\-]/gi, '_').replace(/_{2,}/g, '_') + '.pdf'
                                    //console.log('clean PDF name', cleanPDFFileName)
                                    fs.writeFile(path.join(pdfSavePath, subDirectory, cleanPDFFileName), data, (error) => {
                                        if(error) {
                                            logError('PDF Write Error:\r\n' + error)
                                            event.returnValue = false
                                        }
                                        event.returnValue = true
                                    })
                                })
                            } else {
                                // direct print
                                var silentMode = store.get('system_settings').print_settings.slentMode
                                var printDeviceName = store.get('system_settings').print_settings.blPrinterDevice ? store.get('system_settings').print_settings.blPrinterDevice : ''

                                // change the printer if the same computer is used to print invoices
                                if(requestParams.printDoc == 'invoice' && store.get('system_settings').print_settings.printInvoiceInTheSameComputer) {
                                    printDeviceName = store.get('system_settings').print_settings.blInvoicePrinterDevice ? store.get('system_settings').print_settings.blInvoicePrinterDevice : ''
                                }

                                prevWindow.webContents.print({silent: silentMode, printBackground: printBackground, deviceName: printDeviceName}, (done) => {
                                    if(!done) {
                                        logError('Print Error:\r\n' + done)
                                        event.returnValue = false
                                    }

                                    event.returnValue = true
                                })
                            }
                            
                        })

                        prevWindow.on('close', () => {
                            prevWindow = null
                        })

                        prevWindow.loadURL(printLayoutURL)

                    })

                    ipcMain.on('save-print-layout', (event, args) => {
                        var pageParams = JSON.parse(`${args}`)
                        var layoutType = pageParams.layoutType
                        var pageSize = pageParams.pageSize

                        var printLayoutURL = process.env.NODE_ENV === 'development'
                            ? `http://localhost:9080/#/save_print_layout/${layoutType}/${pageSize}`
                            : `file://${__dirname}/index.html#save_print_layout/${layoutType}/${pageSize}`

                        if(layoutType === 'medi_report') {
                            printLayoutURL = process.env.NODE_ENV === 'development'
                            ? `http://localhost:9080/#/save_print_layout_medi_report/${layoutType}/${pageSize}`
                            : `file://${__dirname}/index.html#save_print_layout_medi_report/${layoutType}/${pageSize}`
                        }

                        let printLayoutWindow = new BrowserWindow({
                            title: "Print Layout Preview",
                            fullscreen: false, 
                            backgroundColor: '#fff',
                            width: width, 
                            height: height,
                            toolbar: false,
                            show: false
                        })

                        if(process.env.NODE_ENV === 'production'  && displayErrors) {
                            printLayoutWindow.webContents.openDevTools()
                        }

                        printLayoutWindow.on('did-finish-load', () => {
                            
                        })

                        printLayoutWindow.loadURL(printLayoutURL)

                        printLayoutWindow.once('ready-to-show', () => {
                            printLayoutWindow.show()
                        })                        
                    })
                }

                
            }
        })

        splashWindow.on('hide', () => {
            splashWindow.webContents.send('close-socket', 'closed')
        })

        splashWindow.on('closed', () => {
            splashWindow = null
        })
    } else {
        // load app setup page
        mainWindow.loadURL(winURL)

        mainWindow.once('ready-to-show', () => {
            mainWindow.show()              
        })
    }

    mainWindow.on('closed', () => {
        mainWindow = null
    })
}

app.on('ready', createWindow)

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow()
  }
})

function checkForActivation() {
    var appActive = true
    var installDate = store.get('installDate')
    if(!installDate || installDate == "") {
        var timestamp = new Date().getTime()
        store.set('installDate', timestamp)
        store.set('activationKey', '')
        store.set('activationCode', '')
    } else if(installDate > 0) {
        // check for trail period
        var installedDate = store.get("installDate")
        var activationKey = store.get("activationKey")
        var nowTimestamp = new Date().getTime()
        if(activationKey == "" && (nowTimestamp - installedDate) > trialPeriod) {
            appActive = false
        } else if(!activationKey || activationKey == "") {
            var daysLeft = parseInt(((installedDate+trialPeriod) - nowTimestamp)/86400000)
            appActive = {status: 'in-trial', daysLeft: daysLeft, serialNo: store.get('appSerial')}
        }
    }

    return appActive
}

function logError(log_entry) {
  if(!fs.existsSync(path.join(ftAppDataDir, 'logs'))) {
    fse.mkdirsSync(path.join(ftAppDataDir, 'logs'))
  }
  
  var date = new Date()
  var log_string = "========================= Main process ==========================\r\n";
  log_string += formatDate(date, true, false) + ": \r\n"; 
  log_string += log_entry + "\r\n\r\n";

  fs.appendFile(ftErrorLogFile, log_string, (err) => {
    console.log(err)
  })
}

function formatDate(dateInput, time=false, hrs24=true) {
    var date = dateInput;
    if(typeof dateInput === 'string') {
        date = new Date(dateInput);
    }
    
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'pm' : 'am';
    minutes = minutes < 10 ? '0'+minutes : minutes;
    var strTime = hours + ':' + minutes + ':00';
    if(!hrs24) {
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        strTime = hours + ':' + minutes + ' ' + ampm;
    }
    
    var dayVal = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();
    var monthVal = date.getMonth()+1 < 10 ? '0' + (date.getMonth()+1) : date.getMonth()+1;
    if(time) {
        return date.getFullYear() + "-" + monthVal + "-" + dayVal + ' ' + strTime;
    } else {
        return date.getFullYear() + "-" + monthVal + "-" + dayVal;
    }
    
}

/**
 * Auto Updater
 *
 * Uncomment the following code below and install `electron-updater` to
 * support auto updating. Code Signing with a valid certificate is required.
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-electron-builder.html#auto-updating
 */

/*
import { autoUpdater } from 'electron-updater'

autoUpdater.on('update-downloaded', () => {
  autoUpdater.quitAndInstall()
})

app.on('ready', () => {
  if (process.env.NODE_ENV === 'production') autoUpdater.checkForUpdates()
})
 */
