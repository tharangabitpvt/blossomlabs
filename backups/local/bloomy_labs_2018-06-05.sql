-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 05, 2018 at 12:39 AM
-- Server version: 5.7.22-0ubuntu0.16.04.1
-- PHP Version: 7.0.30-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bloomy_labs`
--

-- --------------------------------------------------------

--
-- Table structure for table `bloomy_collecting_centers`
--

CREATE TABLE `bloomy_collecting_centers` (
  `id` int(11) NOT NULL,
  `center_name` varchar(255) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bloomy_collecting_centers`
--

INSERT INTO `bloomy_collecting_centers` (`id`, `center_name`, `address`, `phone`, `email`, `published`) VALUES
(1, 'Jaya medi labs', '123, Palawatta road, Alpitiya', '772995537', 'abcd@bbb.com', 1),
(3, 'Chamal medi lab', 'Meegahatenna', '9876543210', NULL, 1),
(4, 'ABC medi lab', 'Galle road, Dodamdoowa', NULL, NULL, 1),
(5, 'Coop Hospital', NULL, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `bloomy_customer`
--

CREATE TABLE `bloomy_customer` (
  `id` int(11) NOT NULL,
  `nic` varchar(15) DEFAULT NULL,
  `customer_title` char(10) NOT NULL,
  `customer_name` varchar(255) NOT NULL,
  `sex` char(1) NOT NULL DEFAULT 'M',
  `dob` date DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `blood_type` varchar(5) DEFAULT NULL,
  `reg_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bloomy_customer`
--

INSERT INTO `bloomy_customer` (`id`, `nic`, `customer_title`, `customer_name`, `sex`, `dob`, `address`, `phone`, `email`, `blood_type`, `reg_date`) VALUES
(3, '822653804v', 'Mr.', 'Tharanga Chaminda Nandasena', 'M', '1982-09-21', 'Malwachchigoda, Meegahatenna', '772995537', 'tharangachai@abc.com', 'O+', '2018-05-14 20:22:00'),
(4, '123456789v', '', 'Sayul Seyhiru', 'M', '2015-07-08', 'Galle', NULL, NULL, 'AB+', '2018-05-14 23:21:00'),
(5, NULL, '', 'Sumudu', 'M', '2018-01-05', NULL, NULL, NULL, 'O+', '2018-05-14 23:47:00'),
(7, NULL, '', 'Kumara', 'M', '2000-05-09', NULL, NULL, NULL, 'O+', '2018-05-14 23:53:00'),
(8, '987654310v', '', 'Mala Saparamadu', 'F', '1990-11-22', 'Elpitiya', NULL, NULL, 'O+', '2018-05-15 00:05:00'),
(9, '556689421v', '', 'Sujani Samanlatha', 'F', '1978-06-02', 'Monaragala', NULL, NULL, 'O+', '2018-05-15 00:49:00'),
(10, NULL, '', 'Chaminda Kulathunga', 'M', '1995-12-31', 'Matugama', NULL, NULL, 'O-', '2018-05-15 13:05:00'),
(11, NULL, '', 'Chaminda Nandasena', 'M', '1995-12-31', 'Matugama', NULL, NULL, 'A-', '2018-05-15 13:18:00'),
(12, NULL, 'Mr.', 'Nandana Gunathilaka', 'M', '1965-06-21', NULL, NULL, NULL, 'O+', '2018-05-15 13:22:00'),
(13, '786541236v', 'Mrs.', 'Kumari Disanayake', 'F', '1978-04-04', 'Pitigala', NULL, NULL, 'A+', '2018-05-23 13:19:00'),
(14, '952654582v', 'Mr.', 'Gunadasa Wedisinghe', 'M', '1995-12-31', NULL, NULL, NULL, 'O+', '2018-05-23 13:45:00'),
(15, NULL, 'Mr.', 'Saman', 'M', '1976-11-18', NULL, NULL, NULL, 'O-', '2018-05-23 14:55:00'),
(16, NULL, 'Miss.', 'Malkanthi', 'F', '1983-09-09', NULL, NULL, NULL, 'O+', '2018-05-23 15:02:00'),
(17, '965842654v', 'Mrs.', 'Maheshi Gunarathan', 'F', '1996-08-23', 'Uragaha', NULL, NULL, 'O+', '2018-05-25 14:49:00'),
(18, NULL, 'Mr.', 'Kumara Thirimadura', 'M', '1968-10-05', 'Colombo', NULL, NULL, 'B+', '2018-05-28 01:08:00'),
(19, NULL, 'Miss.', 'Shanthi Bandara', 'F', '1995-03-02', 'Ginigathhena', NULL, NULL, 'O+', '2018-05-28 01:10:00'),
(20, NULL, 'Mr.', 'Malaka Sirimaanna', 'M', '1982-09-22', NULL, NULL, NULL, 'O+', '2018-05-28 01:13:00'),
(21, NULL, 'Mr.', 'Gunarathna', 'M', '1996-06-02', NULL, NULL, NULL, 'O+', '2018-05-28 01:15:00'),
(22, NULL, 'Mr.', 'Malaka Gunadinghe', 'M', '1978-12-24', 'Polgampola', NULL, NULL, 'O+', '2018-05-28 09:24:00'),
(23, NULL, 'Mr.', 'Dinesh Kumara', 'M', '1990-05-14', NULL, NULL, NULL, 'O+', '2018-05-28 09:41:00'),
(24, NULL, 'Mr.', 'Nirmala Kothalawala', 'M', '1950-05-23', NULL, NULL, NULL, 'O+', '2018-05-28 10:31:00'),
(25, NULL, 'Baby.', 'Supun Ranjana', 'M', '2018-01-04', NULL, NULL, NULL, 'O+', '2018-05-28 11:06:00'),
(26, NULL, 'Miss.', 'Jayanthaa Hapugoda', 'F', '1962-10-10', 'Ambalangoda', NULL, NULL, 'O+', '2018-05-28 11:16:00'),
(27, NULL, 'Mr.', 'Jayawardana', 'M', '1970-06-03', NULL, NULL, NULL, 'O+', '2018-05-28 11:27:00'),
(28, NULL, 'Mr.', 'Duminda Mahesh', 'M', '1983-11-11', NULL, NULL, NULL, 'AB+', '2018-05-28 11:29:00'),
(29, NULL, 'Mr.', 'Namal Gamage', 'M', '1976-01-31', NULL, NULL, NULL, 'O+', '2018-05-28 11:32:00'),
(30, '856582541v', 'Mrs.', 'Dhanushka Damayanthi', 'F', '1985-08-25', 'Pitigala', NULL, NULL, 'O-', '2018-05-29 20:34:00'),
(31, NULL, 'Mr.', 'Mahesh Kumara', 'M', '2001-11-25', NULL, NULL, NULL, 'O+', '2018-05-31 16:36:00'),
(32, NULL, 'Baby.', 'Shehan Mihiranga', 'M', '2016-02-15', NULL, NULL, NULL, 'A-', '2018-05-31 22:34:00'),
(33, NULL, 'Mr.', 'Namal Gunasekara', 'M', '1988-09-21', '#120, Pitigala road, Elpitiya', NULL, NULL, '', '2018-06-03 08:17:00'),
(34, '952653804v', 'Mr.', 'Buddika Kumara', 'M', '1995-01-01', 'Meegahatenna', '9876543210', 'buddhika@abc.com', 'A+', '2018-06-03 08:37:00'),
(35, NULL, 'Mr.', 'Milinga Moragoda', 'M', '1964-08-16', NULL, NULL, NULL, 'O-', '2018-06-03 08:43:00'),
(36, NULL, 'Mrs.', 'Ganga Sudharshanee', 'F', '1980-10-25', NULL, NULL, NULL, 'O+', '2018-06-03 08:48:00'),
(37, NULL, 'Miss.', 'Hirunika Premachandra', 'F', '1978-11-15', NULL, NULL, NULL, NULL, '2018-06-03 08:53:00'),
(38, NULL, 'Ven.', 'Sumanathissa himi', 'M', '1982-11-28', NULL, NULL, NULL, NULL, '2018-06-03 09:01:00'),
(39, NULL, 'Mr.', 'Gamage Don', 'M', '1984-12-25', NULL, NULL, NULL, 'B+', '2018-06-03 09:17:00'),
(40, NULL, 'Mr.', 'Eranga Namal', 'M', '1982-09-21', NULL, NULL, NULL, 'O+', '2018-06-03 15:08:00');

-- --------------------------------------------------------

--
-- Table structure for table `bloomy_fluid_types`
--

CREATE TABLE `bloomy_fluid_types` (
  `id` int(11) NOT NULL,
  `fluid_type_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bloomy_fluid_types`
--

INSERT INTO `bloomy_fluid_types` (`id`, `fluid_type_name`) VALUES
(1, 'Blood'),
(2, 'Blood plasma'),
(3, 'Blood serum'),
(4, 'Breast milk'),
(5, 'Pus'),
(6, 'Saliva'),
(7, 'Semen'),
(8, 'Sputum'),
(9, 'Urine'),
(12, 'cccc'),
(13, 'www');

-- --------------------------------------------------------

--
-- Table structure for table `bloomy_invoice`
--

CREATE TABLE `bloomy_invoice` (
  `id` int(11) NOT NULL,
  `invoice_no` varchar(50) NOT NULL,
  `collecting_center` int(11) NOT NULL,
  `colleted_date` date NOT NULL,
  `ref_doctor` int(11) DEFAULT NULL,
  `customer_age` varchar(50) NOT NULL,
  `customer_height` float NOT NULL,
  `customer_weight` float NOT NULL,
  `customer_id` int(11) NOT NULL,
  `price` float NOT NULL,
  `pay_type` tinyint(1) NOT NULL DEFAULT '1',
  `discount` float NOT NULL,
  `paid_amount` float NOT NULL,
  `invoice_time` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bloomy_invoice`
--

INSERT INTO `bloomy_invoice` (`id`, `invoice_no`, `collecting_center`, `colleted_date`, `ref_doctor`, `customer_age`, `customer_height`, `customer_weight`, `customer_id`, `price`, `pay_type`, `discount`, `paid_amount`, `invoice_time`, `user_id`, `status`) VALUES
(3, '18-AA-000001', 3, '2018-04-01', 1, '{"years":35,"months":7,"days":10}', 165, 65, 3, 2250, 1, 0, 500, '2018-04-01 00:00:00', 1, 2),
(4, '18-AA-000002', 5, '2018-04-10', 5, '{"years":2,"months":10,"days":10}', 0, 0, 4, 2300, 0, 0, 0, '2018-04-10 00:00:00', 1, 2),
(5, '18-AA-000003', 4, '2018-04-10', 5, '{"years":0,"months":4,"days":10}', 0, 0, 5, 1500, 0, 0, 0, '2018-04-10 00:00:00', 1, 2),
(6, '18-AA-000004', 3, '2018-05-14', 1, '{"years":18,"months":0,"days":10}', 0, 0, 7, 750, 0, 0, 0, NULL, 1, 2),
(7, '18-AA-000005', 3, '2018-05-15', 1, '{"years":27,"months":5,"days":10}', 0, 0, 8, 800, 1, 0, 0, NULL, 1, 2),
(8, '18-AA-000006', 3, '2018-05-15', 6, '{"years":39,"months":11,"days":10}', 0, 0, 9, 3050, 0, 0, 3500, '2018-05-15 08:14:05', 1, 2),
(9, '18-AA-000007', 3, '2018-05-15', 5, '{"years":22,"months":4,"days":10}', 0, 0, 11, 2300, 1, 10, 0, '2018-05-15 13:18:00', 1, 2),
(10, '18-AA-000008', 3, '2018-05-15', 6, '{"years":52,"months":10,"days":10}', 0, 0, 12, 1260, 0, 0, 2000, '2018-05-15 13:22:00', 1, 0),
(12, '18-AA-000009', 4, '2018-05-23', 7, '{"years":35,"months":8,"days":2}', 0, 0, 3, 1260, 1, 50, 0, '2018-05-23 12:43:00', 1, 0),
(13, '18-AA-000010', 1, '2018-05-23', 1, '{"years":40,"months":1,"days":19}', 0, 0, 13, 1210, 1, 3, 0, '2018-05-23 13:19:00', 1, 0),
(14, '18-AA-000011', 3, '2018-05-23', 5, '{"years":22,"months":4,"days":23}', 0, 0, 14, 1960, 1, 5, 0, '2018-05-23 13:45:00', 1, 0),
(15, '18-AA-000012', 3, '2018-05-23', NULL, '{"years":41,"months":6,"days":5}', 150, 70, 15, 1960, 0, 0, 1500, '2018-05-23 14:55:00', 1, 0),
(16, '18-AA-000013', 1, '2018-05-23', NULL, '{"years":34,"months":8,"days":14}', 0, 0, 16, 3510, 1, 0, 0, '2018-05-23 15:02:00', 1, 0),
(17, '18-AA-000014', 3, '2018-05-25', NULL, '{"years":21,"months":9,"days":2}', 0, 0, 17, 2250, 1, 0, 0, '2018-05-25 14:49:00', 1, 0),
(18, '18-AA-000015', 3, '2018-05-28', NULL, '{"years":49,"months":7,"days":23}', 0, 0, 18, 750, 1, 0, 0, '2018-05-28 01:08:00', 1, 0),
(19, '18-AA-000016', 3, '2018-05-28', NULL, '{"years":23,"months":2,"days":26}', 0, 0, 19, 1260, 1, 0, 0, '2018-05-28 01:10:00', 1, 0),
(20, '18-AA-000017', 5, '2018-05-28', NULL, '{"years":35,"months":8,"days":6}', 0, 0, 20, 460, 1, 0, 0, '2018-05-28 01:13:00', 1, 0),
(21, '18-AA-000018', 4, '2018-05-28', NULL, '{"years":21,"months":11,"days":26}', 0, 0, 21, 460, 1, 0, 0, '2018-05-28 01:15:00', 1, 0),
(22, '18-AA-000019', 1, '2018-05-28', 1, '{"years":39,"months":5,"days":12}', 0, 0, 22, 1260, 0, 5, 500, '2018-06-05 00:20:00', 1, 0),
(23, '18-AA-000020', 3, '2018-05-28', 5, '{"years":28,"months":0,"days":14}', 0, 0, 23, 460, 1, 0, 0, '2018-05-28 09:41:00', 1, 0),
(24, '18-AA-000021', 3, '2018-05-28', 8, '{"years":68,"months":0,"days":5}', 0, 0, 24, 460, 1, 0, 0, '2018-05-28 10:31:00', 1, 0),
(25, '18-AA-000022', 1, '2018-05-28', 8, '{"years":0,"months":4,"days":24}', 0, 0, 25, 460, 1, 0, 0, '2018-05-28 11:06:00', 1, 0),
(26, '18-AA-000023', 1, '2018-05-28', 8, '{"years":55,"months":7,"days":18}', 0, 0, 26, 460, 1, 0, 0, '2018-05-28 11:16:00', 1, 0),
(27, '18-AA-000024', 4, '2018-05-28', 6, '{"years":47,"months":11,"days":25}', 0, 0, 27, 460, 1, 0, 0, '2018-05-28 11:27:00', 1, 0),
(28, '18-AA-000025', 4, '2018-05-28', 7, '{"years":34,"months":6,"days":17}', 0, 0, 28, 1500, 1, 0, 0, '2018-05-28 11:29:00', 1, 2),
(29, '18-AA-000026', 5, '2018-05-28', NULL, '{"years":42,"months":3,"days":28}', 0, 0, 29, 1500, 1, 0, 0, '2018-05-28 11:32:00', 1, 0),
(30, '18-AA-000027', 3, '2018-05-28', NULL, '{"years":35,"months":8,"days":7}', 0, 0, 3, 1500, 1, 0, 0, '2018-05-28 16:01:00', 1, 1),
(31, '18-AA-000028', 3, '2018-05-29', 7, '{"years":32,"months":9,"days":4}', 0, 0, 30, 2250, 0, 5, 1500, '2018-05-29 20:34:00', 1, 0),
(32, '18-AA-000029', 5, '2018-05-31', 8, '{"years":16,"months":6,"days":6}', 0, 0, 31, 1500, 1, 0, 0, '2018-05-31 16:36:00', 1, 0),
(33, '18-AA-000030', 3, '2018-05-31', 7, '{"years":2,"months":3,"days":16}', 0, 0, 32, 2250, 0, 20, 500, '2018-05-31 22:34:00', 1, 0),
(34, '18-AA-000031', 1, '2018-06-03', 7, '{"years":29,"months":8,"days":13}', 0, 0, 33, 2000, 1, 0, 0, '2018-06-03 08:17:00', 1, 0),
(35, '18-AA-000032', 3, '2018-06-03', 7, '{"years":23,"months":5,"days":2}', 0, 0, 34, 2000, 1, 0, 0, '2018-06-03 08:37:00', 1, 0),
(36, '18-AA-000033', 5, '2018-06-03', NULL, '{"years":53,"months":9,"days":18}', 0, 0, 35, 2000, 1, 0, 0, '2018-06-03 08:43:00', 1, 0),
(37, '18-AA-000034', 1, '2018-06-03', 8, '{"years":37,"months":7,"days":9}', 0, 0, 36, 950, 1, 0, 0, '2018-06-03 08:48:00', 1, 0),
(38, '18-AA-000035', 4, '2018-06-03', 6, '{"years":39,"months":6,"days":19}', 0, 0, 37, 2000, 1, 0, 0, '2018-06-03 08:53:00', 1, 0),
(39, '18-AA-000036', 4, '2018-06-03', 6, '{"years":35,"months":6,"days":6}', 0, 0, 38, 2000, 1, 0, 0, '2018-06-03 09:01:00', 1, 0),
(40, '18-AA-000037', 1, '2018-06-03', NULL, '{"years":33,"months":5,"days":9}', 0, 0, 39, 2750, 1, 0, 0, '2018-06-03 09:17:00', 1, 0),
(41, '18-AA-000038', 1, '2018-06-03', 7, '{"years":35,"months":8,"days":13}', 0, 0, 40, 1550, 1, 10, 0, '2018-06-03 15:08:00', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `bloomy_invoice_test`
--

CREATE TABLE `bloomy_invoice_test` (
  `id` int(11) NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `profile_id` int(11) NOT NULL,
  `package_id` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `report_date` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bloomy_invoice_test`
--

INSERT INTO `bloomy_invoice_test` (`id`, `invoice_id`, `profile_id`, `package_id`, `status`, `report_date`, `user_id`) VALUES
(1, 3, 4, NULL, 2, '2018-04-01 16:45:00', 1),
(2, 3, 2, NULL, 2, '2018-04-01 12:43:00', 1),
(3, 4, 4, NULL, 2, '2018-04-10 12:55:00', 1),
(4, 4, 7, NULL, 2, '2018-05-22 19:57:00', 1),
(5, 5, 4, NULL, 2, '2018-05-22 18:58:00', 1),
(6, 6, 2, NULL, 2, '2018-05-22 19:43:00', 1),
(7, 7, 7, NULL, 2, '2018-05-22 01:33:00', 1),
(8, 8, 7, NULL, 2, '2018-05-22 12:48:00', 2),
(9, 8, 2, NULL, 2, '2018-06-03 15:27:00', 1),
(10, 8, 4, NULL, 2, '2018-05-22 17:15:00', 2),
(11, 9, 4, NULL, 2, '2018-05-22 19:54:00', 2),
(12, 9, 7, NULL, 2, '2018-05-22 19:56:00', 2),
(13, 10, 7, NULL, 2, '2018-05-24 13:48:00', 1),
(14, 10, 8, NULL, 0, NULL, NULL),
(15, 12, 7, NULL, 2, '2018-05-23 21:00:00', 1),
(16, 12, 8, NULL, 0, NULL, NULL),
(17, 13, 2, NULL, 1, '2018-05-25 12:47:00', 1),
(18, 13, 8, NULL, 0, NULL, NULL),
(19, 14, 4, NULL, 0, NULL, NULL),
(20, 14, 8, NULL, 0, NULL, NULL),
(21, 15, 4, NULL, 0, NULL, NULL),
(22, 15, 8, NULL, 0, NULL, NULL),
(23, 16, 8, NULL, 0, NULL, NULL),
(24, 16, 2, NULL, 0, NULL, NULL),
(25, 16, 4, NULL, 0, NULL, NULL),
(26, 16, 7, NULL, 0, NULL, NULL),
(27, 17, 2, NULL, 0, NULL, NULL),
(28, 17, 4, NULL, 0, NULL, NULL),
(29, 18, 2, NULL, 0, NULL, NULL),
(30, 19, 7, NULL, 0, NULL, NULL),
(31, 19, 8, NULL, 0, NULL, NULL),
(32, 20, 8, NULL, 0, NULL, NULL),
(33, 21, 8, NULL, 0, NULL, NULL),
(36, 23, 8, NULL, 0, NULL, NULL),
(37, 24, 8, NULL, 0, NULL, NULL),
(38, 25, 8, NULL, 0, NULL, NULL),
(39, 26, 8, NULL, 0, NULL, NULL),
(40, 27, 8, NULL, 0, NULL, NULL),
(41, 28, 4, NULL, 2, '2018-05-28 13:00:00', 1),
(42, 29, 4, NULL, 0, NULL, NULL),
(43, 30, 4, NULL, 1, '2018-05-29 18:22:00', 1),
(44, 31, 2, NULL, 0, NULL, NULL),
(45, 31, 4, NULL, 0, NULL, NULL),
(46, 32, 4, NULL, 0, NULL, NULL),
(47, 33, 4, NULL, 0, NULL, NULL),
(48, 33, 2, NULL, 0, NULL, NULL),
(49, 34, 4, 1, 0, NULL, NULL),
(50, 34, 7, 1, 0, NULL, NULL),
(51, 35, 4, 1, 0, NULL, NULL),
(52, 35, 7, 1, 0, NULL, NULL),
(53, 36, 4, 1, 0, NULL, NULL),
(54, 36, 7, 1, 0, NULL, NULL),
(55, 37, 2, 2, 0, NULL, NULL),
(56, 37, 8, 2, 0, NULL, NULL),
(57, 38, 4, 1, 0, NULL, NULL),
(58, 38, 7, 1, 0, NULL, NULL),
(59, 39, 4, 1, 0, NULL, NULL),
(60, 39, 7, 1, 0, NULL, NULL),
(61, 40, 4, 1, 0, NULL, NULL),
(62, 40, 7, 1, 0, NULL, NULL),
(63, 40, 2, NULL, 2, '2018-06-03 09:20:00', 1),
(64, 41, 2, NULL, 0, NULL, NULL),
(65, 41, 7, NULL, 0, NULL, NULL),
(92, 22, 8, NULL, 0, NULL, NULL),
(93, 22, 7, NULL, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `bloomy_invoice_test_result`
--

CREATE TABLE `bloomy_invoice_test_result` (
  `invoice_test_id` int(11) NOT NULL,
  `test_id` int(11) NOT NULL,
  `result` varchar(50) DEFAULT NULL,
  `ref_range` varchar(25) DEFAULT NULL,
  `remark` varchar(20) NOT NULL,
  `remark_key` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bloomy_invoice_test_result`
--

INSERT INTO `bloomy_invoice_test_result` (`invoice_test_id`, `test_id`, `result`, `ref_range`, `remark`, `remark_key`) VALUES
(7, 10, '150', '<500', 'Low', NULL),
(8, 10, '100', '<500', 'Low', NULL),
(10, 3, '100', '80-120', 'Normal', NULL),
(10, 5, '1500', '<30000', 'Positive', NULL),
(10, 11, NULL, NULL, 'Positive', NULL),
(5, 3, '110', '80-120', 'Normal', NULL),
(5, 5, '5200', '<30000', 'Positive', NULL),
(5, 11, NULL, NULL, 'Positive', NULL),
(6, 3, '150', '80-120', 'Positive', NULL),
(6, 5, '25.5', '<30000', 'Positive', NULL),
(6, 4, '24.9', '200-500', 'Low', NULL),
(11, 3, '100', '80-120', 'Normal', NULL),
(11, 5, '250', '<30000', 'Positive', NULL),
(11, 11, NULL, NULL, 'Negetive', NULL),
(12, 10, '150', '<500', 'Low', NULL),
(4, 10, '550', '<500', 'High', NULL),
(15, 10, '600', '<500', 'High', NULL),
(2, 3, '100', '80-120', 'Normal', NULL),
(2, 5, '10.6', '<30000', 'Positive', NULL),
(2, 4, '17.88', '200-500', 'Low', NULL),
(13, 10, '675', '<500', 'High', NULL),
(1, 3, '100', '80-120', 'Normal', NULL),
(1, 5, '50000', '<30000', 'Negetive', NULL),
(1, 11, NULL, NULL, 'Positive', NULL),
(17, 3, '100', '80-120', 'Normal', NULL),
(17, 5, '50', '<30000', 'Positive', NULL),
(17, 4, '10', '200-500', 'Low', NULL),
(3, 3, '100', '80-120', 'Normal', NULL),
(3, 5, '1500', '<30000', 'Positive', NULL),
(3, 11, NULL, NULL, 'Positive', NULL),
(41, 3, '100', '80-120', 'Normal', NULL),
(41, 5, '500', '<30000', 'Positive', NULL),
(41, 11, NULL, NULL, 'Negetive', NULL),
(43, 3, '100', '80-120', 'Normal', 'in_range'),
(43, 5, '1500', '<30000', 'Positive', 'low'),
(43, 11, NULL, NULL, 'Negetive', 'low'),
(63, 3, '100', '80-120', 'Negetive', 'low'),
(63, 5, '1', '<30000', 'Positive', 'low'),
(63, 4, '19.8', '200-500', 'Low', 'low'),
(9, 3, '100', '80-120', 'Normal', NULL),
(9, 5, '15.5', '<30000', 'Positive', NULL),
(9, 4, '16.9', '200-500', 'Low', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `bloomy_packages`
--

CREATE TABLE `bloomy_packages` (
  `id` int(11) NOT NULL,
  `package_name` varchar(100) NOT NULL,
  `price` float NOT NULL,
  `published` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bloomy_packages`
--

INSERT INTO `bloomy_packages` (`id`, `package_name`, `price`, `published`) VALUES
(1, 'Full body checkup', 2000, 1),
(2, 'Sample Package 1', 950, 1);

-- --------------------------------------------------------

--
-- Table structure for table `bloomy_package_profile`
--

CREATE TABLE `bloomy_package_profile` (
  `package_id` int(11) NOT NULL,
  `profile_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bloomy_package_profile`
--

INSERT INTO `bloomy_package_profile` (`package_id`, `profile_id`) VALUES
(2, 2),
(2, 8),
(1, 4),
(1, 7);

-- --------------------------------------------------------

--
-- Table structure for table `bloomy_profiles`
--

CREATE TABLE `bloomy_profiles` (
  `id` int(11) NOT NULL,
  `profile_name` varchar(100) NOT NULL,
  `description` text,
  `price` float NOT NULL,
  `fluid_type` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bloomy_profiles`
--

INSERT INTO `bloomy_profiles` (`id`, `profile_name`, `description`, `price`, `fluid_type`) VALUES
(2, 'Test profile 1', '<p>This is refrence notes</p><p>line 1</p><p>line 2</p>', 750, 7),
(4, 'Lipid profile', '<p>Reference data</p><p>Line 1</p><p>Line2</p><p><strong>Line3</strong></p><p>Line 4</p>', 1500, 5),
(7, 'sample test 6', '<p>asdas</p><p>asdas asdas</p><p>asdas</p>', 800, 9),
(8, 'Sample Profile 7', '<p>asdas</p><p>adasd</p><p>asdasdas asd asd</p>', 460, 4);

-- --------------------------------------------------------

--
-- Table structure for table `bloomy_ref_doctors`
--

CREATE TABLE `bloomy_ref_doctors` (
  `id` int(11) NOT NULL,
  `doc_title` char(10) NOT NULL DEFAULT 'Dr.',
  `doc_name` varchar(255) NOT NULL,
  `specialty` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bloomy_ref_doctors`
--

INSERT INTO `bloomy_ref_doctors` (`id`, `doc_title`, `doc_name`, `specialty`, `description`) VALUES
(1, 'PROF', 'Samarajeew Bandara De Mel', 'CARDIAOTHORACIC SURGEON', 'MBBS col SL'),
(5, 'PROF', 'Gunasena Hapuarachchi', 'NEURO SURGEON', NULL),
(6, 'Dr', 'Samanmali Kularathna', 'GENERAL SURGEON', NULL),
(7, 'Dr', 'Karunadheera Egodawela', 'CARDIOLOGIST', NULL),
(8, 'PROF', 'Sudharshanee Gamade', 'EYE SURGEON', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `bloomy_ref_ranges`
--

CREATE TABLE `bloomy_ref_ranges` (
  `id` int(11) NOT NULL,
  `test_id` int(11) NOT NULL,
  `sex` char(5) NOT NULL DEFAULT 'any',
  `age_limit` varchar(5) NOT NULL DEFAULT 'any',
  `ref_range` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bloomy_ref_ranges`
--

INSERT INTO `bloomy_ref_ranges` (`id`, `test_id`, `sex`, `age_limit`, `ref_range`) VALUES
(19, 9, 'any', 'any', '> 250'),
(43, 2, 'M', '<10m', '120-150'),
(44, 2, 'F', '<12m', '<150'),
(45, 5, 'any', 'any', '<30000'),
(46, 3, 'any', 'any', '80-120'),
(48, 7, 'any', 'any', '< 250'),
(49, 8, 'any', 'any', '< 260'),
(50, 4, 'any', 'any', '200-500'),
(51, 6, 'M', 'any', '50-150'),
(52, 6, 'F', 'any', '70-170'),
(54, 10, 'any', 'any', '<500');

-- --------------------------------------------------------

--
-- Table structure for table `bloomy_stock_item`
--

CREATE TABLE `bloomy_stock_item` (
  `id` int(11) NOT NULL,
  `item_name` varchar(100) NOT NULL,
  `unit` varchar(50) NOT NULL,
  `remain` float DEFAULT NULL,
  `threshold` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bloomy_stock_item`
--

INSERT INTO `bloomy_stock_item` (`id`, `item_name`, `unit`, `remain`, `threshold`) VALUES
(1, 'HIV stick', 'sticks', 31, 15),
(6, 'item 5', 'g', -2, 2),
(8, 'item 7', 'sticks', 23, 20),
(9, 'item 8', 'mg', 18, 50),
(10, 'item 9', 'g', 120, 151),
(11, 'itam 11', 'm<sup>3</sup>', 600, 500);

-- --------------------------------------------------------

--
-- Table structure for table `bloomy_tests`
--

CREATE TABLE `bloomy_tests` (
  `id` int(11) NOT NULL,
  `test_name` varchar(100) NOT NULL,
  `remark_values` varchar(255) NOT NULL,
  `remark_colors` varchar(255) NOT NULL DEFAULT '{"low":null,"in_range":null,"high":null}',
  `unit` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bloomy_tests`
--

INSERT INTO `bloomy_tests` (`id`, `test_name`, `remark_values`, `remark_colors`, `unit`) VALUES
(2, 'Test 1', '{"low":"Low","in_range":"Normal","high":"High"}', '{"low":"#007bff","in_range":"#28a745","high":"#dc3545"}', 'mm<sup>2</sup>'),
(3, 'Test 2 test', '{"low":"Negetive","in_range":"Normal","high":"Positive"}', '{"low":"#007bff","in_range":"#28a745","high":"#dc3545"}', 'g/dl'),
(4, 'Test 4', '{"low":"Low","in_range":"Normal","high":"High"}', '{"low":"#007bff","in_range":"#28a745","high":"#dc3545"}', 'u/l'),
(5, 'Test 5', '{"low":"Positive","in_range":null,"high":"Negetive"}', '{"low":"#28a745","in_range":"", "high":"#dc3545"}', 'g/dl'),
(6, 'sample test 1', '{"low":"Low","in_range":"Normal","high":"High"}', '{"low":"#007bff","in_range":"#28a745","high":"#dc3545"}', 'mg/dl'),
(7, 'sample test 2', '{"low":"Positive","in_range":null,"high":"Negetive"}', '{"low":"#dc3545","in_range":"","high":"#28a745"}', 'u/l'),
(8, 'sample test 4', '{"low":"Low","in_range":null,"high":"High"}', '{"low":"#dc3545","in_range":"","high":"#28a745"}', '/HDL'),
(9, 'sample test 5', '{"low":"Normal","in_range":null,"high":"High"}', '{"low":null,"in_range":null,"high":null}', 'mm<sup>2</sup>'),
(10, 'sample test 6', '{"low":"Low","in_range":null,"high":"High"}', '{"low":"","in_range":"","high":""}', 'mm<sup>2</sup>'),
(11, 'Test no ref range', '{"low":"Negetive","in_range":null,"high":"Positive"}', '{"low":"#28a745","in_range":"","high":"#dc3545"}', 'mg/dl');

-- --------------------------------------------------------

--
-- Table structure for table `bloomy_tests_profiles`
--

CREATE TABLE `bloomy_tests_profiles` (
  `profile_id` int(11) NOT NULL,
  `test_id` int(11) NOT NULL,
  `calc_formula` varchar(255) DEFAULT NULL,
  `item_order` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bloomy_tests_profiles`
--

INSERT INTO `bloomy_tests_profiles` (`profile_id`, `test_id`, `calc_formula`, `item_order`) VALUES
(4, 3, '', 0),
(4, 5, '', 0),
(4, 11, '', 0),
(7, 10, NULL, 0),
(8, 2, '', 0),
(8, 7, '(#9# - #2# )/5', 0),
(8, 9, '', 0),
(2, 3, '', 0),
(2, 5, '', 0),
(2, 4, '(#3# - #5#)/5', 0);

-- --------------------------------------------------------

--
-- Table structure for table `bloomy_test_stock_item`
--

CREATE TABLE `bloomy_test_stock_item` (
  `test_id` int(11) NOT NULL,
  `stock_item_id` int(11) NOT NULL,
  `units_per_test` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bloomy_test_stock_item`
--

INSERT INTO `bloomy_test_stock_item` (`test_id`, `stock_item_id`, `units_per_test`) VALUES
(2, 1, 1),
(3, 10, 2.5),
(3, 9, 3),
(10, 8, 2);

-- --------------------------------------------------------

--
-- Table structure for table `bloomy_units`
--

CREATE TABLE `bloomy_units` (
  `id` int(11) NOT NULL,
  `unit_label` varchar(10) NOT NULL,
  `unit` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bloomy_units`
--

INSERT INTO `bloomy_units` (`id`, `unit_label`, `unit`) VALUES
(1, 'mm2', 'mm<sup>2</sup>'),
(5, '/HDL', '/HDL'),
(6, 'mm3', 'mm<sup>3</sup>'),
(7, 'mg/dl', 'mg/dl'),
(8, 'u/l', 'u/l'),
(9, 'cm', 'cm'),
(11, 'cm3', 'cm<sup>3</sup>'),
(12, 'm3', 'm<sup>3</sup>');

-- --------------------------------------------------------

--
-- Table structure for table `bloomy_user`
--

CREATE TABLE `bloomy_user` (
  `id` int(11) NOT NULL,
  `f_name` varchar(255) NOT NULL,
  `l_name` varchar(255) DEFAULT NULL,
  `username` varchar(255) NOT NULL,
  `pwd` varchar(255) NOT NULL,
  `user_type` tinyint(1) NOT NULL DEFAULT '1',
  `slmc_reg_no` varchar(10) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL,
  `create_date` datetime NOT NULL,
  `last_login` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bloomy_user`
--

INSERT INTO `bloomy_user` (`id`, `f_name`, `l_name`, `username`, `pwd`, `user_type`, `slmc_reg_no`, `email`, `phone`, `is_active`, `create_date`, `last_login`) VALUES
(1, 'Tharanga', 'Nandasena', 'admin', '17e883064e247bfcaa8402fb05abdfd5a68d8ff7bbceffc950528c206a26b38d', 0, '2256', 'tharangachaminda@gmail.com', '772995537', 1, '2018-04-23 00:00:00', '2018-05-02 14:04:58'),
(2, 'Sayul', 'Seyhiru', 'sayul', 'e1a5204b40477d660429cf432d0f85d266467b7e42b64c4780867baf9e5bb745', 1, '15325', NULL, NULL, 1, '2018-04-24 00:00:00', '2018-04-30 22:38:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bloomy_collecting_centers`
--
ALTER TABLE `bloomy_collecting_centers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bloomy_customer`
--
ALTER TABLE `bloomy_customer`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_nic` (`nic`);

--
-- Indexes for table `bloomy_fluid_types`
--
ALTER TABLE `bloomy_fluid_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bloomy_invoice`
--
ALTER TABLE `bloomy_invoice`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `u_invoice_no` (`invoice_no`);

--
-- Indexes for table `bloomy_invoice_test`
--
ALTER TABLE `bloomy_invoice_test`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bloomy_packages`
--
ALTER TABLE `bloomy_packages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bloomy_profiles`
--
ALTER TABLE `bloomy_profiles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bloomy_ref_doctors`
--
ALTER TABLE `bloomy_ref_doctors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bloomy_ref_ranges`
--
ALTER TABLE `bloomy_ref_ranges`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bloomy_stock_item`
--
ALTER TABLE `bloomy_stock_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bloomy_tests`
--
ALTER TABLE `bloomy_tests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bloomy_units`
--
ALTER TABLE `bloomy_units`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bloomy_user`
--
ALTER TABLE `bloomy_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bloomy_collecting_centers`
--
ALTER TABLE `bloomy_collecting_centers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `bloomy_customer`
--
ALTER TABLE `bloomy_customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT for table `bloomy_fluid_types`
--
ALTER TABLE `bloomy_fluid_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `bloomy_invoice`
--
ALTER TABLE `bloomy_invoice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT for table `bloomy_invoice_test`
--
ALTER TABLE `bloomy_invoice_test`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=94;
--
-- AUTO_INCREMENT for table `bloomy_packages`
--
ALTER TABLE `bloomy_packages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `bloomy_profiles`
--
ALTER TABLE `bloomy_profiles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `bloomy_ref_doctors`
--
ALTER TABLE `bloomy_ref_doctors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `bloomy_ref_ranges`
--
ALTER TABLE `bloomy_ref_ranges`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;
--
-- AUTO_INCREMENT for table `bloomy_stock_item`
--
ALTER TABLE `bloomy_stock_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `bloomy_tests`
--
ALTER TABLE `bloomy_tests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `bloomy_units`
--
ALTER TABLE `bloomy_units`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `bloomy_user`
--
ALTER TABLE `bloomy_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
